package postgres

import (
	"fmt"
	pb "github/FIrstService/template-service/product-service/User/genproto"
	"log"

	"github.com/jmoiron/sqlx"
)

type userRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...

func NewUserRepo(db *sqlx.DB) *userRepo {
	return &userRepo{db: db}
}

func (r *userRepo) Create(user *pb.User) (*pb.User, error) {
	userResp := pb.User{}
	err := r.db.QueryRow(`insert into owners (firstname,lastname,password,email,code,username) values ($1,$2,$3,$4,$5,$6) returning id,firstname,lastname,password,email,code,username`, user.Firstname, user.Lastname, user.Password, user.Email, user.Code, user.Username).Scan(
		&userResp.Id, &userResp.Firstname, &userResp.Lastname, &userResp.Password, &userResp.Email, &userResp.Code, &userResp.Username)
	if err != nil {
		return &pb.User{}, err
	}
	return &userResp, nil
}

func (r *userRepo) GetUser(ids *pb.ID) (*pb.GetUsers, error) {
	response := &pb.GetUsers{}
	for _, id := range ids.Id {
		tempUser := &pb.User{}
		err := r.db.QueryRow(`select * from owners where id=$1`, id).Scan(&tempUser.Id, &tempUser.Firstname, &tempUser.Lastname, &tempUser.Password, &tempUser.Email, &tempUser.Code, &tempUser.Username)
		if err != nil {
			log.Fatal("Error while select owners", err)
		}
		response.Users = append(response.Users, tempUser)
	}
	return response, nil
}

func (r *userRepo) CheckField(field, value string) (*pb.CheckFieldResp, error) {
	query := fmt.Sprintf("select 1 from owners where %s = %s", field, value)
	var exists int
	err := r.db.QueryRow(query).Scan(&exists)
	if err != nil {
		return &pb.CheckFieldResp{}, err
	}

	if exists == 0 {
		return &pb.CheckFieldResp{Exists: false}, nil
	}
	return &pb.CheckFieldResp{Exists: true}, nil
}

func (r *userRepo) DeleteUser(ids *pb.ID) error {
	for _, id := range ids.Id {
		_, err := r.db.Exec(`DELETE FROM owners WHERE id=$1`, id)
		if err != nil {
			log.Fatal("Error while delete owners", err)
		}
	}
	return nil
}
