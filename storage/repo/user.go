package repo

import (
	pb "github/FIrstService/template-service/product-service/User/genproto"
)

// UserStorageI ...
type UserStorageI interface {
	Create(*pb.User) (*pb.User, error)
	DeleteUser(*pb.ID) error
	GetUser(*pb.ID) (*pb.GetUsers, error)
	CheckField(field, value string) (*pb.CheckFieldResp, error)
}
