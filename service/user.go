package service

import (
	"context"
	pb "github/FIrstService/template-service/product-service/User/genproto"
	l "github/FIrstService/template-service/product-service/User/pkg/logger"
	"github/FIrstService/template-service/product-service/User/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// UserService ...
type UserService struct {
	storage storage.IStorage
	logger  l.Logger
}

// NewUserService ...

func NewUserService(db *sqlx.DB, log l.Logger) *UserService {
	return &UserService{
		storage: storage.NewStoragePg(db),
		logger:  log,
	}
}

func (s *UserService) Create(ctx context.Context, req *pb.User) (*pb.User, error) {
	user, err := s.storage.User().Create(req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error insert user", err))
		return &pb.User{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return user, nil
}

func (s *UserService) GetUser(ctx context.Context, req *pb.ID) (*pb.GetUsers, error) {
	_, err := s.storage.User().GetUser(req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error select users", err))
		return &pb.GetUsers{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return &pb.GetUsers{}, nil
}

func (s *UserService) CheckField(ctx context.Context, req *pb.CheckFieldReq) (*pb.CheckFieldResp, error) {
	user, err := s.storage.User().CheckField(req.Field, req.Value)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error insert user", err))
		return &pb.CheckFieldResp{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return user, nil
}

func (s *UserService) DeleteUser(ctx context.Context, req *pb.ID) (*pb.Empty, error) {
	err := s.storage.User().DeleteUser(req)
	if err != nil {
		s.logger.Error("Error while delete users", l.Any("Delete", err))
		return &pb.Empty{}, status.Error(codes.InvalidArgument, "wrong id for delete")
	}
	return &pb.Empty{}, nil
}
